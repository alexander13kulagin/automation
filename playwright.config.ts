import { PlaywrightTestConfig } from '@playwright/test';

const config: PlaywrightTestConfig = {
  testMatch: /.*.ts/,
  timeout: 30 * 10000,
  retries: 0,
  workers: 1,
  reporter: [['list'], ['html', { outputFolder: 'htmlReport' }]],
  use: {
    baseURL: 'https://web.network.ixela.dev/',
    browserName: 'chromium',
    // proxy: {
    //   server: 'http://165.227.130.151:33176',
    //   username: process.env.PROXY_TEST_USERNAME,
    //   password: process.env.PROXY_TEST_PASSWORD,
    // },
    viewport: { width: 1600, height: 1200 },
    video: 'retain-on-failure',
    screenshot: 'only-on-failure',
    headless: false,
    permissions: ['clipboard-read', 'clipboard-write'],
  },
  expect: {
    timeout: 35000,
    toMatchSnapshot: {
      maxDiffPixels: 30
    }
  },
    outputDir: 'test-results/',
};

export default config;
