import { test, expect } from '@playwright/test';

test('check login', async ({ page }) => {

const emailForLogin = '';
const passwordForLogin = '';

  await page.goto('https://web.network.ixela.dev/');
  await page.pause();
  await page.locator('input[name="email"]').fill(emailForLogin);
  await page.locator('input[name="password"]').fill(passwordForLogin);
  await page.locator('button[type="submit"]').click();

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle('Elain');
});
